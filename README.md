# Shell minitools

**A collection of small shell scripts that make your life easier.**

## Scripts

- **`audioctl.sh`:** A lightweight interface to control pulseaudio from your window manager.
- **`dotfiler.sh`:** Keep your dotfiles updated in a repository (**WIP**).
- **`e.sh`:** A shortcut to open paths in your favourite file explorer.
- **`photodate.sh`:** Copy your photos to files with their creation date as their name.
- **`rc.sh`:** A shortcut to open your settings (`rc`) files.
- **`review-remove.sh` (Arch Linux only):** Interactively review your system packages and choose which ones to remove.

## Installation

You can install any of these scripts in `$HOME/.local/bin/`.
It is recommended that you install them without the `.sh` extension to make it easier to call them:

```sh
cp src/rc.sh ~/.local/bin/rc
```

You can also install them all at once:

```sh
cd src
for i in *
do
	cp "$i" "~/.local/bin/$(echo "$i" | sed 's/\.sh$//g')"
done
```

You may need to add `~/.local/bin/` to your PATH.
To do so, add this line to your shell rc (e.g. `.bashrc` or `.zshrc`):

```sh
PATH="$PATH:$HOME/.local/bin"
```

## Documentation

All scripts are written independently of each other so that they can be distributed by themselves.
You can read the `print_help` functions at the top of the scripts to learn about them.

#!/usr/bin/env sh

# Globals {{{1
device_id=0
max_volume=100
volume_step=5
# }}}1

# print_help {{{1
# Print the help text and exit.

print_help ()
{
cat << EOF
AUDIOCTL
========
A lightweight interface to control pulseaudio from your window manager.
--------------------------------------------------------------------------

:: DEPENDENCIES

- libpulse: Operate the audio interface.
- pamixer: Get the current volume.

:: USAGE

  audioctl.sh toggle_mic # Toggle the microphone input on and off.
  audioctl.sh toggle_audio # Toggle the audio output on and off.
  audioctl.sh volume_down # Turn the volume down $step%.
  audioctl.sh volume_up# Turn the volume up $step% up until $max_volume.

You can change the volume steps and the max volume above.

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit
}
# }}}1

# toggle_mic {{{1
# Mute and unmute the microphone.

toggle_mic ()
{
	pactl set-source-mute "$device_id" toggle
}
# }}}1

# toggle_audio {{{1
# Mute and unmute the audio output.

toggle_audio ()
{
	pactl set-sink-mute "$device_id" toggle
}
# }}}1

# volume_down {{{1
# Thurn the volume down according to $volume_step.

volume_down ()
{
	pactl set-sink-volume "$device_id" "-$volume_step%"
}
# }}}1

# volume_up {{{1
# Turn the volume up according to $volume_step without exceding $max_volume.

volume_up ()
{
	volume="$(pamixer --get-volume)"

	if [ $((volume + volume_step)) -gt $max_volume ]
	then
		pactl set-sink-volume "$device_id" "$max_volume%"
	else
		pactl set-sink-volume "$device_id" "+$volume_step%"
	fi
}
# }}}1

main ()
{
	query="$(echo "$1" | tr -d '[:blank:]')"
	$query
}

main "$@"

# vim:ft=sh:ts=3:sw=0:noet

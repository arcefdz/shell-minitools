#!/usr/bin/env sh

# Globals {{{1
explorer="ranger"
path="."
prefix="$HOME/"
# }}}1

# print_help {{{1
# Print the help text and exit.

print_help ()
{
cat << EOF
E
=
A shortcut to open paths in your favourite file explorer.
--------------------------------------------------------------------------

:: USAGE

  e.sh
  e.sh [-h]
  e.sh [PATH]
  e.sh [SHORTCUT]

The script will first try to find a path with the first argument provided. If it
can't find it, it will lookup the query functions and set the path accordingly
if it finds a match. You can define your own query functions below. If no
arguments are provided, the script behaves like "e.sh .", i.e., opens the
working directory.

  SHORT ARG    LONG ARG              DESCRIPTION
  ------------------------------------------------
  -h           --help                Print this help text and exit.

You can define your explorer above. All paths are prefixed by "~/" to make it
easier to update the script.

:: USAGE EXAMPLES

  e.sh pic # Opens "~/Pictures".
  e.sh vim # Opens "~/.config/nvim".
  e.sh ~/Pictures # Opens "~/Pictures". Useful if "pic" is not defined below.

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit
}
# }}}1

# Query functions {{{1
# Each function corresponds to a file path. Calling `e` passing the name of one
# of these functions will run them. For example, `e doc` will run `doc()` and
# then the main function will open $HOME/Documents.

conf () { path=".config";                    };
doc  () { path="Documents";                  };
dow  () { path="Downloads";                  };
loc  () { path=".local/share";               };
mus  () { path="Music";                      };
pic  () { path="Pictures";                   };
tel  () { path="Downloads/Telegram Desktop"; };
vid  () { path="Videos";                     };
vim  () { path=".config/nvim";               };
# }}}1

# abort () {{{1
# Closes the program with an error message.

abort ()
{
	query="$1"
	printf "\033[1;31mCouldn't find \"%s\" and it's not a path.\033[0m\n" "$query"
	exit 1
}
# }}}1

main ()
{
	if [ $# -eq 0 ]
	then
		prefix=""
	else
		query="$1"

		if [ "$query" = "-h" ] || [ "$query" = "--help" ]
		then
			print_help
		elif [ -d "$query" ]
		then
			path="$query"
			prefix=""
		else
			query="$(echo "$query" | tr '[:upper:]' '[:lower:]')"
			$query 1>/dev/null 2>&1 || abort "$query"
		fi
	fi

	$explorer "$prefix$path"
}

main "$@"

# vim:ft=sh:ts=3:sw=0:noet

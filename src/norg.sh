#!/usr/bin/env sh


# Constants
ROOT="$HOME/.local/norg"
ERROR_WORKSPACE="__ERROR__"
NVIM="nvim"


# print_quick_help
# Print a short version of the help text and exit with $fabort.

print_quick_help ()
{
cat << EOF
Usage:
  norg [-hl] [-g] <workspace>


Operational arguments:
  -h, --help    Print a complete help text and exit.
  -g, --git     Record your changes with git after exiting.
  -l, --list    List all available workspaces.
EOF
exit "$fabort"
}

# print_help
# Print the help text and exit with $fabort.

print_help ()
{
cat << EOF
NORG
====
Access your Neorg workspaces with ease
----------------------------------------

:: DEPENDENCIES

- nvim: Requires that Neorg is installed: https://github.com/nvim-neorg/neorg

:: USAGE

  norg [-hl] [-g] <workspace>

With no arguments the script will load the default "index.norg" file located
at the root of your workspace container directory. This script expects that all
your Norg workspaces are located in a single directory, which you can change
by setting the "\$ROOT" constant at the top of the script.

Workspaces are directories located in $ROOT.
When passing a workspace as the first argument, the script will find it case
insensitively and will open it unless there are multiple workspaces that
match the name in a case insensitive manner.


  SHORT ARG      LONG ARG    DESCRIPTION
  ----------------------------------------
  -h             --help      Print this help text and exit.

  -g             --git       Record your changes with git after exiting.

  -l             --list      List all available workspaces.

  <workspace>                The workspace to open Neorg with.

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit "$fabort"
}


#Flags
fabort=0
fgit=0
fhelp=0
flist=0


commit_changes ()
{
	cd "$ROOT" || exit 2

	if ! [ -d ".git" ]
	then
		git init
	fi

	files_added="$(git add -v . 2>/dev/stderr)"

	if [ -n "$files_added" ]
	then
		git commit -m "$(create_commit_message)"
		remote_exists="$(git remote | wc -l)"

		if [ "$remote_exists" != "0" ]
		then
			git push
		fi
	fi

	cd - >/dev/null || exit 2
}


create_commit_message ()
{
	echo "[NORG.SH] Incremental changes: $(date '+%F %T')"
}


find_workspace ()
{
	workspace=""

	while [ $# -gt 0 ] && [ -z "$workspace" ]
	do
		case "$1" in
		-*);;
		*) workspace="$1";;
		esac
		shift
	done

	exact_match="$(find "$ROOT" -mindepth 1 -maxdepth 1 -type d -name "$workspace" | wc -l)"

	if [ "$exact_match" = "0" ]
	then
		any_match="$(find "$ROOT" -mindepth 1 -maxdepth 1 -type d -iname "$workspace")"
		any_match_count="$(echo "$any_match" | wc -l)"

		if [ "$any_match_count" -eq 1 ]
		then
			workspace="$(basename "$any_match")"
		elif [ "$any_match_count" -gt 1 ]
		then
			echo "Multiple matches were found:" > /dev/stderr
			echo "$any_match" > /dev/stderr
			workspace="$ERROR_WORKSPACE"
		fi
	fi

	echo "$workspace"
}


list_workspaces ()
{
	echo "Workspaces available at $ROOT:"

	find "$ROOT" -mindepth 1 -maxdepth 1 -type d | while read -r line
	do
		workspace="$(basename "$line")"
		description="$(parse_description "$line")"

		if [ "$description" = "" ]
		then
			echo "- $workspace"
		else
			echo "- $workspace: $description"
		fi
	done
}


open_norg ()
{
	requested_workspace="$1"

	if [ -z "$requested_workspace" ]
	then
		cd "$ROOT" || exit 1
		$NVIM _ "+bdel" "+NeorgStart" "+Telescope neorg switch_workspace"
		cd - >/dev/null || exit 1
	else
		$NVIM _ "+bdel" "+NeorgStart" "+Neorg workspace $1"
	fi
}


parse_args ()
{
	while [ $# -gt 0 ]
	do
		case "$1"
		in
		-g|--git)
			fgit=1
		;;
		-h|--help)
			fhelp=1
		;;
		-l|--list)
			flist=1
		;;
		-*)
			printf "\033[31;1mUnrecognised argument: \033[0m%s\n" "$1"
			fabort=1
		;;
		*)
			requested_workspace="$1"
		;;
		esac

		shift
	done

	if [ $fhelp -eq 1 ]
	then
		print_help
	elif [ $fabort -ne 0 ]
	then
		print_quick_help
	fi
}


parse_description ()
{
	workspace="$1"

	description=""
	index="$workspace/index.norg"
	index_exists="$(find "$index" 2>/dev/null | wc -l)"

	if [ "$index_exists" = "1" ]
	then
		document_meta_line="$(grep -n "@document.meta" "$index" | sed 's/:.*//g')"

		if [ "$document_meta_line" != "" ]
		then
			cut_index="$(tail -n "+$document_meta_line" "$index")"
			end_line="$(echo "$cut_index" | grep -n "@end" | sed 's/:.*//g')"
			cut_index="$(echo "$cut_index" | head -n "$end_line")"

			description="$(echo "$cut_index" | grep description | sed 's/^[^:]\+: *//g')"
		fi
	fi

	echo "$description"
}


main ()
{
	parse_args "$@"

	if [ $flist -eq 1 ]
	then
		list_workspaces
	else
		open_norg "$requested_workspace"
	fi

	if [ $fgit -eq 1 ]
	then
		commit_changes
	fi
}


main "$@"

#!/usr/bin/env sh

# Flags {{{1
fabort=0   # An error has been found and the program must abort.
fhelp=0    # Print the help text and exit.
# }}}1

# Globals {{{1
NODATE="NODATE"
extension="jpg"
to_convert=""
to_convert_size=0
maxdepth=1
# }}}1

# print_quick_help {{{1
# Print a short version of the help text and exit with $fabort.

print_quick_help ()
{
cat << EOF
Usage: photodate.sh [-h]

Operational arguments:
  -h, --help                        Print a complete help text and exit.
  -e [EXT], --extension=[EXT]       Convert "*.EXT" files.
  -m [DEPTH], --maxdepth=[DEPTH]    Set the maximum depth to search for files.
EOF
exit "$fabort"
}
# }}}1

# print_help {{{1
# Print the help text and exit with $fabort.

print_help ()
{
cat << EOF
PHOTODATE
=========
Convert all your photography files' names to their original date
--------------------------------------------------------------------------

:: DEPENDENCIES

- imagemagick: Get metadata from the photography files.

:: USAGE

  photodate.sh [-h]

With no arguments the script will search for all "*.jpg" files in the current
directory and will copy them into "./photodate-converted/". You can customise
its behaviour with the following arguments:

  SHORT ARG    LONG ARG              DESCRIPTION
  ------------------------------------------------
  -h           --help                Print this help text and exit.

  -e [EXT]     --extension=[EXT]     Convert "*.EXT" files.

  -m [DEPTH]   --maxdepth=[DEPTH]    Set the maximum depth to search for files.
                                     Defaults to 1 (the current directory). If
                                     the maximum depth is higher than one,
                                     photodate will search for files DEPTH-1
                                     directories deep.

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit "$fabort"
}
# }}}1

# Printf wrappers {{{1
# A small interface to show program status information to the user with printf.

printf_bigarrow ()
{
	printf "\033[32;1m==> \033[0;1m%s\033[0m%s\n" "$1" "$2"
}

printf_error ()
{
	printf "\033[31;1m:: %s!\033[0m%s\n" "$1" "$2"
}

printf_smallarrow ()
{
	printf "\033[35;1m  -> \033[0;1m%s\033[0m%s\n" "$1" "$2"
}
# }}}1

# build_filename {{{1
# Encapsulatees the creation of the filename for the converted files. Accepts an
# optional second argument for the file index. If it's present, it adds it in
# parens before the extension.

build_filename ()
{
	creation_date="$1"
	file_index="$2"
	filename=""

	if [ "$file_index" = "" ]
	then
		filename="IMG_$creation_date.$extension"
	else
		filename="IMG_$creation_date($file_index).$extension"
	fi

	echo "$filename"
}
# }}}1

# convert_files {{{1
# Converts all phography files with the given extension in the current directory
# into files with their creation date as their filename in "./converted/".

convert_files ()
{
	counter=1
	mkdir -p "./converted/"

	echo "$to_convert" | while read -r file
	do
		printf_bigarrow "[$counter/$to_convert_size] " "Parsing $file..."
		creation_date="$(get_creation_date "$file")"

		if [ "$creation_date" = "$NODATE" ]
		then
			printf_error "No date was found"
		else
			printf_smallarrow "Found creation date: " "$creation_date"
		fi

		printf_smallarrow "Converting file..."
		converted_filename="$(get_converted_filename "$creation_date")"
		cp "$file" "converted/$converted_filename"

		: $((counter += 1))
		echo
	done
}
# }}}1

# find_files {{{1
# Find the photography files in the current directory excluding those in the
# output directory. They are both saved in global variables for the script.

find_files ()
{
	to_convert="$(\
		find . \
			-maxdepth "$maxdepth" \
			-name "*.$extension" \
		| grep -v "^\./converted"
	)"
	to_convert_size=$(echo "$to_convert" | wc -l)
}
# }}}1

# get_converted_filename {{{1
# Creates the filename for the converted photography. If there are other files
# with the same name it appends the number in parens before the extension. The
# original file is also renamed with "(0)"

get_converted_filename ()
{
	creation_date="$1"
	similar_files="$(find ./converted -name "IMG_$creation_date*.$extension")"
	filename="$(build_filename "$creation_date")"

	if [ "$similar_files" != "" ]
	then
		similar_files_size=$(echo "$similar_files" | wc -l)

		if [ "$similar_files_size" -eq 1 ]
		then
			renamed_filename="$(build_filename "$creation_date" "0")"
			mv "./converted/$filename" "./converted/$renamed_filename"
		fi

		filename="$(build_filename "$creation_date" "$similar_files_size")"
	fi

	echo "$filename"
}
# }}}1

# get_creation_date {{{1
# Retrieves the creation date from a photography file's metadata. If it can't
# find the date, it looks for other approximate alternatives. If no date can
# be found, the date is set to "$NODATE".

get_creation_date ()
{
	file="$1"
	metadata="$(identify -verbose "$file")"

	# Try with exif metadata first
	creation_date="$(\
		echo "$metadata" \
		| grep exif:DateTimeOriginal \
		| sed -e 's/^ *exif:DateTimeOriginal: //g' \
		| tr -d ':'  \
		| tr ' ' '_' \
	)"

	# Try with modification date if exif date was not found
	if [ -z "$creation_date" ]
	then
		creation_date="$(\
			echo "$metadata" \
			| grep date:modify \
			| sed -e 's/^ *date:modify: //g' -e 's/\+.*$//g' \
			| tr -d '-' \
			| tr -d ':' \
			| tr 'T' '_' \
		)"
	fi

	# If all metadata lookups failed, there's no date to be retrieved
	if [ -z "$creation_date" ]
	then
		creation_date="$NODATE"
	fi

	echo "$creation_date"
}
# }}}1

# parse_args {{{1
# Set program flags depending on the arguments passed from the shell.
# Unrecognised arguments are aditionally displayed to the user.

parse_args ()
{
	while [ $# -gt 0 ]
	do
		case "$1"
		in
		-h|--help)
			fhelp=1
		;;
		-e)
			extension="$2"
			shift
		;;
		--extension=*)
			extension="$(echo "$1" | sed 's/^[^=]\+=//g')"
		;;
		-m)
			maxdepth=$2
			shift
		;;
		--maxdepth=*)
			maxdepth=$(echo "$1" | sed 's/^[^=]\+=//g')
		;;
		*)
			printf "\033[31;1mUnrecognised argument: \033[0m%s\n" "$1"
			fabort=1
		;;
		esac

		shift
	done

	if [ $fhelp -eq 1 ]
	then
		print_help
	elif [ $fabort -ne 0 ]
	then
		print_quick_help
	fi
}
# }}}1

main ()
{
	parse_args "$@"
	find_files
	convert_files
}

main "$@"

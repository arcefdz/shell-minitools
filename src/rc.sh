#!/usr/bin/env sh

# Globals {{{1
editor="nvim"
file="$0"
prefix="$HOME/"
# }}}1

# print_help {{{1
# Print the help text and exit.

print_help ()
{
cat << EOF
RC
==
A shortcut to open your settings ("rc") files.
--------------------------------------------------------------------------

:: USAGE

  rc.sh
  rc.sh [-h]
  rc.sh [SHORTCUT]

Without arguments, the script will open itself. With one argument, it will
lookup the query functions and set the file path accordingly if it finds a
match, then open it. You can define your own query functions below.

  SHORT ARG    LONG ARG              DESCRIPTION
  ------------------------------------------------
  -h           --help                Print this help text and exit.

You can define your editor above. All paths are prefixed by "~/" to make it
easier to update the script.

:: USAGE EXAMPLES

  rc.sh vim # Opens "~/.config/nvim/init.vim".

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit
}
# }}}1

# Query functions {{{1
# Each function corresponds to a file path. Calling `rc` passing the name of one
# of these functions will run them. For example, `rc zsh` will run `zsh()` and
# then the main function will open $HOME/.zshrc.

e        () { file=".local/bin/e";             }
kitty    () { file=".config/kitty/kitty.conf"; }
ranger   () { file=".config/ranger/rc.conf";   }
newsboat () { file=".config/newsboat/urls";    }
vim      () { file=".config/nvim/init.vim";    }
zsh      () { file=".zshrc";                   }
# }}}1

# abort () {{{1
# Closes the program with an error message.

abort ()
{
	query="$1"
	printf "\033[1;31mCouldn't find rc for \"%s\".\033[0m\n" "$query"
	exit 1
}
# }}}1

main ()
{
	query="$(echo "$1" | tr '[:upper:]' '[:lower:]')"

	$query 1>/dev/null 2>&1 || abort "$query"
	if [ "$file" = "$0" ]; then prefix=""; fi

	$editor "$prefix$file"
}

main "$@"

# vim:ft=sh:ts=3:sw=0:noet

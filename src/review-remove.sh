#!/usr/bin/env sh

# Flags {{{1
fabort=0   # An error has been found and the program must abort.
fdeps=0    # Review packages installed as dependencies.
fforeign=0 # Only review foreign packages.
fhelp=0    # Print the help text and exit.
fnative=0  # Only review native packages.
fquick=0   # Show packages in a quick list style.
# }}}1

# Globals {{{1
packages_to_remove=""
pacman="pacman"
# }}}1

# print_quick_help {{{1
# Print a short version of the help text and exit with $fabort.

print_quick_help ()
{
cat << EOF
Usage: review-remove.sh [-h] (-[dmnq])*
Call the script with no arguments for the default functionality.

Operational arguments:
  -h, --help          Print a complete help text and exit.
  -d, --dependencies  Review packages installed as dependencies.
  -m, --foreign-only  Review foreign packages only, reset if -n is also set.
  -n, --native-only   Review native packages only., reset if -m is also set.
  -q, --quick-list    Review packages using a quick list style.
EOF
exit "$fabort"
}
# }}}1

# print_help {{{1
# Print the help text and exit with $fabort.

print_help ()
{
cat << EOF
REVIEW - REMOVE
===============
Interactively review your system packages and choose which ones to remove.
--------------------------------------------------------------------------

:: DEPENDENCIES

- pacman: Manage your system packages.

:: USAGE

  review-remove.sh [-h] (-[dmnq])*

Call the script with no arguments for the default functionality. You can
customise its behaviour with the following arguments:

  SHORT ARG    LONG ARG          DESCRIPTION
  --------------------------------------------
  -h           --help            Print this help text and exit.

  -d           --dependencies    Review packages installed as dependencies.

  -m           --foreign-only    Review foreign packages only, e.g. those
                                 installed from the AUR.

  -n           --native-only     Review native packages only, i.e. those
                                 installed from the official repositories.

  -q           --quick-list      Review packages using a quick list style
                                 instead of showing relevant information
                                 for every package.

The options -m and -n will be ignored if they're both set.

:: LICENCE

Licenced under the GNU General Public License v3.0:
  https://www.gnu.org/licenses/gpl-3.0.en.html
  https://gitlab.com/Groctel/shell-minitools

THERE IS NO WARRANTY FOR THE PROGRAM, TO THE EXTENT PERMITTED BY APPLICABLE LAW.
EXCEPT WHEN OTHERWISE STATED IN WRITING THE COPYRIGHT HOLDERS AND/OR OTHER
PARTIES PROVIDE THE PROGRAM "AS IS" WITHOUT WARRANTY OF ANY KIND, EITHER
EXPRESSED OR IMPLIED, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE ENTIRE RISK AS TO THE
QUALITY AND PERFORMANCE OF THE PROGRAM IS WITH YOU. SHOULD THE PROGRAM PROVE
DEFECTIVE, YOU ASSUME THE COST OF ALL NECESSARY SERVICING, REPAIR OR CORRECTION.
EOF
exit "$fabort"
}
# }}}1

# Array strings functions {{{1
# Arrays strings are strings that contain a series of elements separated by the
# comma (',') character. These functions form a small interface over these
# strings to make the program easier to read and write.

array_front ()
{
	echo "$1" | sed 's/,.*//g'
}

array_pop ()
{
	echo "$1" | sed 's/^,\?[^,]*,\? *//g'
}
# }}}1

# Printf wrappers {{{1
# A small interface to show program status information to the user with printf.

printf_bigarrow ()
{
	printf "\033[32;1m==> \033[0;1m%s\033[0m%s\n" "$1" "$2"
}

printf_error ()
{
	printf "\033[31;1m:: %s!\033[0m%s\n" "$1" "$2"
}

printf_smallarrow ()
{
	printf "\033[35;1m  -> \033[0;1m%s\033[0m%s\n" "$1" "$2"
}
# }}}1

# confirm {{{1
# Queries the user for a yes/no question. If "yes" or "no" are passed before the
# question string, that answer will be the default (i.e. the one answered if the
# user inputs an empty string. This function loops until the user inputs a
# correct answer.

confirm ()
{
	answer=-1

	case "$1" in
	[Yy][Ee][Ss])
		printf "\033[1;32m:: \033[0m%s? \033[1;32m[Y/n]:\033[0m " "$2"
	;;
	[Nn][Oo])
		printf "\033[1;31m:: \033[0m%s? \033[1;31m[y/N]:\033[0m " "$2"
	;;
	*)
		printf "\033[1;33m:: \033[0m%s? \033[1;33m[y/n]:\033[0m " "$1"
	;;
	esac

	while [ $answer -eq -1 ]
	do
		read -r yn

		case $yn in
		[Yy]*)
			answer=0
		;;
		[Nn]*)
			answer=1;
		;;
		*)
			case "$1" in
			[Yy][Ee][Ss])
				answer=0
			;;
			[Nn][Oo])
				answer=1
			;;
			esac
		;;
		esac
	done

	return $answer
}
# }}}1

# get_attribute  {{{1
# Parses the package's information string and returns the attribute's value.

get_attribute ()
{
	query="$1"
	info="$2"

	echo "$info" | grep "$query" | sed 's/^[^:]\+: //g'
}
# }}}1

# parse_args {{{1
# Set program flags depending on the arguments passed from the shell.
# Unrecognised arguments are aditionally displayed to the user.

parse_args ()
{
	while [ $# -gt 0 ]
	do
		case "$1"
		in
		-d|--dependencies)
			fdeps=1
		;;
		-h|--help)
			fhelp=1
		;;
		-m|--foreign-only)
			fforeign=1
		;;
		-n|--native-only)
			fnative=1
		;;
		-q|--quick-list)
			fquick=1
		;;
		*)
			printf "\033[31;1mUnrecognised argument: \033[0m%s\n" "$1"
			fabort=1
		;;
		esac

		shift
	done

	if [ $fhelp -eq 1 ]
	then
		print_help
	elif [ $fabort -ne 0 ]
	then
		print_quick_help
	fi
}
# }}}1

# remove_packages {{{1
# Uses $pacman to remove the packages the user has selected for removal. If
# $pacman is the actual "pacman" package, then it prepends sudo to the command.

remove_packages ()
{
	printf_bigarrow "Starting $pacman to remove selected packages..."

	if [ "$pacman" = "pacman" ]
	then
		# shellcheck disable=SC2086
		sudo $pacman -Rcns $packages_to_remove
	else
		# shellcheck disable=SC2086
		$pacman -Rcns $packages_to_remove
	fi
}
# }}}1

# review_packages {{{1
# Fetches the packages explicitly installed by the user and asks the user
# whether they want to remove them in alphabetical order.

review_packages ()
{
	printf_bigarrow "Starting package review"
	printf_smallarrow "Fetching system package list..."

	options="$(set_pacman_options)"
	installed_packages="$($pacman -Q"$options" | sed 's/ .*//g' | tr '\n' ',')"

	while [ -n "$installed_packages" ]
	do
		package="$(array_front "$installed_packages")"
		installed_packages="$(array_pop "$installed_packages")"

		if [ $fquick -eq 0 ]
		then
			show_package_attributes "$package"
		fi

		if confirm "NO" "Remove $package"
		then
			packages_to_remove="$packages_to_remove $package"
		fi
	done
}
# }}}1

# set_pacman_options {{{1
# Set the options used by pacman to query the package list.

set_pacman_options ()
{
	options=""

	if [ $fdeps -eq 0 ]
	then
		options="$options""e"
	fi

	if ! { [ $fforeign -eq 1 ] && [ $fnative -eq 1 ] ;}
	then
		if [ $fforeign -eq 1 ]
		then
			options="$options""m"
		fi

		if [ $fnative -eq 1 ]
		then
			options="$options""n"
		fi
	fi

	echo "$options"
}
# }}}1

# show_attribute {{{1
# Gets an attribute from the package's description and displays it only if it's
# not null, i.e. it equals "None".

show_attribute ()
{
	name="$1"
	info="$2"
	value="$(get_attribute "$name" "$info")"

	if [ "$value" != "None" ]
	then
		printf_smallarrow "$name: " "$value"
	fi
}
# }}}1

# show_package_attributes {{{1
# Displays the package's attributes in a easy to read way for the user.

show_package_attributes ()
{
	package="$1"
	info="$($pacman -Qi "$package")"

	name="$(get_attribute "Name" "$info")"
	size="$(get_attribute "Installed Size" "$info")"
	version="$(get_attribute "Version" "$info")"

	echo ""
	printf_bigarrow "$name $version ($size)"

	show_attribute "Description" "$info"
	show_attribute "Groups" "$info"
	show_attribute "Provides" "$info"
	show_attribute "Required By" "$info"
	show_attribute "Optional For" "$info"
	show_attribute "Conflicts With" "$info"
	show_attribute "Replaces" "$info"
}
# }}}1

main ()
{
	parse_args "$@"
	review_packages

	if [ -n "$packages_to_remove" ]
	then
		remove_packages
	else
		printf_bigarrow "No packages were selected for removal."
	fi
}

main "$@"

# vim:ft=sh:ts=3:sw=0:noet
